/*  Released under BSD 3-Clause "New" or "Revised" License
 *  Copyright (c) 2018, Vitor Flavio Fernandes Ferreira
 *  All rights reserved.
 *
 *  https://spdx.org/licenses/BSD-3-Clause.html
 */

#ifndef ANQ_CONFIGURATION_CONFIGURATIONPARSER_H
#define ANQ_CONFIGURATION_CONFIGURATIONPARSER_H

int parse_config();

#endif
